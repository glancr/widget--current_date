module CurrentDate
  class Engine < ::Rails::Engine
    isolate_namespace CurrentDate
    config.generators.api_only = true

    DEFAULT_STYLES = { font_size: 130 }.freeze
  end
end
